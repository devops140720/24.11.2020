import logging # does not require installation
import datetime

def init(fname, log_level):
    if log_level == 'DEBUG':
        logging.basicConfig(filename=fname, level=logging.DEBUG)
    elif log_level == 'INFO':
        logging.basicConfig(filename=fname, level=logging.INFO)
    elif log_level == 'WARN':
        logging.basicConfig(filename=fname, level=logging.WARN)
    elif log_level == 'ERROR':
        logging.basicConfig(filename=fname, level=logging.ERROR)
    elif log_level == 'FATAL':
        logging.basicConfig(filename=fname, level=logging.FATAL)

def write_lo_log(msg, log_level):
    if log_level == 'DEBUG':
        logging.debug(f'{msg} {datetime.datetime.now()}');
    elif log_level == 'INFO':
        logging.info(f'{msg} {datetime.datetime.now()}');
    elif log_level == 'WARN':
        logging.warning(f'{msg} {datetime.datetime.now()}');
    elif log_level == 'ERROR':
        logging.error(f'{msg} {datetime.datetime.now()}');
    elif log_level == 'FATAL':
        logging.fatal(f'{msg} {datetime.datetime.now()}');
